//
//  NewsAPI.swift
//  Tinkoff News
//
//  Created by Ионин Константин on 18/02/2018.
//  Copyright © 2018 Ионин Константин. All rights reserved.
//

import Foundation

class NewsAPI {
    
    static private var baseURLComponents: NSURLComponents {
        // create "https://api.tinkoff.ru/v1/" URL
        let urlComponents = NSURLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "api.tinkoff.ru"
        urlComponents.path = "/v1/"
        return urlComponents
    }
    
    static func createURLForNewsFeed() -> URL {
        let urlComponents = baseURLComponents
        urlComponents.path?.append("news")
        return urlComponents.url!
    }
    
    static func createURLForNewsContent(itemID: Int) -> URL {
        let urlComponents = baseURLComponents
        urlComponents.path?.append("news_content")
        let idQuery = URLQueryItem(name: "id", value: "\(itemID)")
        urlComponents.queryItems = [idQuery]
        return urlComponents.url!
    }
}
