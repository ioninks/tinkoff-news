//
//  DataManager.swift
//  Tinkoff News
//
//  Created by Ионин Константин on 18/02/2018.
//  Copyright © 2018 Ионин Константин. All rights reserved.
//

import Foundation

enum DataManagerError: Error {
    
    case failedRequest
    case invalidResponse
    case unknown
}

enum DataManagerNewsFeedResult {
    
    case error(DataManagerError)
    case data([NewsFeedItemInfo])
}

enum DataManagerNewsContentResult {
    
    case error(DataManagerError)
    case data(NewsItemContentInfo)
}

final class DataManager {
    
    typealias NewsFeedDataCompletion = (DataManagerNewsFeedResult) -> ()
    
    typealias NewsItemContentCompletion = (DataManagerNewsContentResult) -> ()
    
    func newsFeedData(completion: @escaping NewsFeedDataCompletion) {
        
        let url = NewsAPI.createURLForNewsFeed()
        print("Requesting news feed data...")
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            self.didFetchNewsFeedData(data: data, response: response, error: error, completion: completion)
        }.resume()
    }
    
    func newsItemContent(forID id: Int, completion: @escaping NewsItemContentCompletion) {
        
        let url = NewsAPI.createURLForNewsContent(itemID: id)
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            self.didFetchNewsContentData(data: data, response: response, error: error, completion: completion)
        }.resume()
    }
    
    private func didFetchNewsFeedData(
        data: Data?,
        response: URLResponse?,
        error: Error?,
        completion: NewsFeedDataCompletion) {
        
        if let _ = error {
            completion(.error(.failedRequest))
        } else if let data = data, let response = response as? HTTPURLResponse {
            if response.statusCode == 200 {
                processNewsFeedData(data: data, comletion: completion)
            } else {
                completion(.error(.failedRequest))
            }
        } else {
            completion(.error(.unknown))
        }
    }
    
    private func didFetchNewsContentData(
        data: Data?,
        response: URLResponse?,
        error: Error?,
        completion: NewsItemContentCompletion) {
        
        if let _ = error {
            completion(.error(.failedRequest))
        } else if let data = data, let response = response as? HTTPURLResponse {
            if response.statusCode == 200 {
                processNewsContentData(data: data, completion: completion)
            } else {
                completion(.error(.failedRequest))
            }
        } else {
            completion(.error(.unknown))
        }
    }
    
    typealias JSON = [String: AnyObject]
    
    private func processNewsFeedData(data: Data, comletion: NewsFeedDataCompletion) {
        if let JSON = (try? JSONSerialization.jsonObject(with: data, options: [])) as? JSON {
            let newsFeedItems = parseNewsFeedItemsArray(fromJSON: JSON)
            if newsFeedItems.count > 0 {
                comletion(.data(newsFeedItems))
            } else {
                comletion(.error(.invalidResponse))
            }
        } else {
            comletion(.error(.invalidResponse))
        }
    }
    
    private func processNewsContentData(data: Data, completion: NewsItemContentCompletion) {
        if let JSON = (try? JSONSerialization.jsonObject(with: data, options: [])) as? JSON,
            let newsItemContent = parseNewsItemContent(fromJSON: JSON) {
                completion(.data(newsItemContent))
        } else {
            completion(.error(.invalidResponse))
        }
    }
    
    private func parseNewsFeedItemsArray(fromJSON JSON: JSON) -> [NewsFeedItemInfo] {
        guard let jsonItems = JSON["payload"] as? [JSON] else { return [] }
        
        let feedItems = jsonItems.map { parseNewsFeedItem(fromJSON: $0) }.flatMap { $0 }
        
        return feedItems
    }
    
    private func parseNewsFeedItem(fromJSON JSON: JSON) -> NewsFeedItemInfo? {
        guard let idString = JSON["id"] as? String,
            let id = Int(idString),
            let text = JSON["text"] as? String,
            let timeInMillis = JSON["publicationDate"]?["milliseconds"] as? Int64
            else { return nil }
        
        let timeInterval = TimeInterval(timeInMillis / 1000)
        let publicationDate = Date(timeIntervalSince1970: timeInterval)
        
        let newsFeedItemInfo = NewsFeedItemInfo(
            id: id,
            text: text,
            publicationDate: publicationDate
        )
        return newsFeedItemInfo
    }
    
    private func parseNewsItemContent(fromJSON JSON: JSON) -> NewsItemContentInfo? {
        guard let payload = JSON["payload"] as? JSON,
            let title = payload["title"] as? JSON,
            let idString = title["id"] as? String,
            let id = Int(idString),
            let text = payload["content"] as? String
            else { return nil }
        
        let newsItemContent = NewsItemContentInfo(id: id, text: text)
        return newsItemContent
    }
    
    
    
    
    
    
}
