//
//  NewsItemContentViewController.swift
//  Tinkoff News
//
//  Created by Ионин Константин on 18/02/2018.
//  Copyright © 2018 Ионин Константин. All rights reserved.
//

import UIKit
import CoreData
import WebKit

class NewsItemContentViewController: UIViewController {
        
    var webView: WKWebView!
    
    var newsItemID: Int!
    var moc: NSManagedObjectContext!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureWebView()
        fetchNewsItemContent()
    }
    
    private var dataManager = DataManager()
    
    private func fetchNewsItemContent() {
        
        if let cachedNewsContent = NewsItemContent.fetch(withID: newsItemID, inContext: moc),
            let contentText = cachedNewsContent.text {
            print("used news content from db")
            let contentInfo = NewsItemContentInfo(
                id: Int(cachedNewsContent.id),
                text: contentText
            )
            updateUI(withNewsContent: contentInfo)
            return
        }
        
        dataManager.newsItemContent(forID: newsItemID) { result in
            print("loading news content")
            switch result {
            case .data(let newsItemContent):
                // update UI and database
                DispatchQueue.main.async { [weak self] in
                    self?.updateUI(withNewsContent: newsItemContent)
                    self?.updateDatabase(withNewsContent: newsItemContent)
                }
            case .error(let error):
                // show status
                print("Fetch data error: \(error)")
            }
        }
    }
    
    private func configureWebView() {
        
        let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
        let userScript = WKUserScript(source: jscript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let contentController = WKUserContentController()
        contentController.addUserScript(userScript)
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.userContentController = contentController
        
        self.webView = WKWebView (frame: .zero , configuration: webConfiguration)
        webView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(webView)
        webView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        webView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        webView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    private func updateUI(withNewsContent newsItemContent: NewsItemContentInfo) {
        
        webView.loadHTMLString(newsItemContent.text, baseURL: nil)
    }
    
    private func updateDatabase(withNewsContent newsItemContentInfo: NewsItemContentInfo) {
        moc?.perform { [moc] in
            _ = NewsItemContent.addNewsItemContent(withInfo: newsItemContentInfo, inContext: moc!)
            
            do {
                try self.moc?.save()
            } catch let error {
                print("Core Data Error: \(error)")
            }
        }
    }
}
