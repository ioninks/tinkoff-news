//
//  NewsFeedTableViewCell.swift
//  Tinkoff News
//
//  Created by Ионин Константин on 18/02/2018.
//  Copyright © 2018 Ионин Константин. All rights reserved.
//

import UIKit

struct NewsFeedCellModel {
    
    let newsItemID: Int?
    let titleText: String?
    let publicationDateText: String?
    
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateStyle = .short
        return formatter
    }()
    
    init(withNewsFeedItem feedItem: NewsFeedItem) {
        newsItemID = Int(feedItem.id)
        titleText = feedItem.text
        if let publicationDate = feedItem.publicationDate {
            publicationDateText = dateFormatter.string(from: publicationDate)
        } else {
            publicationDateText = nil
        }
    }
}

class NewsFeedTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    var model: NewsFeedCellModel! {
        didSet {
            titleLabel.text = model.titleText
            dateLabel.text = model.publicationDateText
        }
    }
    
}
