//
//  NewsFeedTableViewController.swift
//  Tinkoff News
//
//  Created by Ионин Константин on 17/02/2018.
//  Copyright © 2018 Ионин Константин. All rights reserved.
//

import UIKit
import CoreData

class NewsFeedTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    
    // MARK: - Constants
    
    private struct Storyboard {
        static let CellIdentifier = "News Feed Cell"
        static let ShowNewsItemContentSegue = "Show News Item Content"
    }
    
    private struct Constants {
        static let LoadingDataErrorTitle = "Не удалось загрузить данные"
        static let LoadingDataErrorMesssage = "Проверьте соединение с интернетом"
    }
    
    // MARK: - Lifecycle and actions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if moc == nil {
            moc = (UIApplication.shared.delegate as? AppDelegate)?.managedObjectContext
        }
        fetchNewsFeedItems()
    }
    
    @IBAction func refresh() {
        fetchNewsFeedItems()
    }
    
    // MARK: - Properties
    
    private var moc: NSManagedObjectContext? { didSet { updateUI() } }

    private var dataManager = DataManager()
    
    var fetchedResultsController: NSFetchedResultsController<NewsFeedItem>? {
        didSet {
            do {
                if let frc = fetchedResultsController {
                    frc.delegate = self
                    try frc.performFetch()
                }
                tableView.reloadData()
            } catch let error {
                print("NSFetchedResultsController.performFetch() failed: \(error)")
            }
        }
    }
    
    // MARK: - Helpers
    
    private func fetchNewsFeedItems() {
        dataManager.newsFeedData { [weak self] result in
            
            switch result {
            case .data(let newsItems):
                DispatchQueue.main.async {
                    self?.refreshControl?.endRefreshing()
                    self?.updateDatabase(withNewsItems: newsItems)
                }
            case .error( _):
                DispatchQueue.main.async {
                    self?.refreshControl?.endRefreshing()
                    self?.showAlert(title: Constants.LoadingDataErrorTitle,
                                    message: Constants.LoadingDataErrorMesssage)
                }
            }
        }
    }
    
    private func updateUI() {
        if let context = moc {
            let request: NSFetchRequest<NewsFeedItem> = NewsFeedItem.fetchRequest()
            
            request.sortDescriptors = [NSSortDescriptor(key: "publicationDate", ascending: false)]
            
            fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        } else {
            fetchedResultsController = nil
        }
    }
    
    private func updateDatabase(withNewsItems newsItemInfos: [NewsFeedItemInfo]) {
        moc?.perform {
            for infoItem in newsItemInfos {
                _ = NewsFeedItem.addNewsFeedItem(withInfo: infoItem, inContext: self.moc!)
            }
            
            do {
                try self.moc?.save()
            } catch let error {
                print("Core Data Error: \(error)")
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Storyboard.ShowNewsItemContentSegue,
            let nicvc = segue.destination as? NewsItemContentViewController,
            let cell = sender as? NewsFeedTableViewCell {
            
            nicvc.newsItemID = cell.model.newsItemID
            nicvc.moc = moc
        }
    }
    
    // MARK: - UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Storyboard.CellIdentifier, for: indexPath) as! NewsFeedTableViewCell
        
        if let newsItem = fetchedResultsController?.object(at: indexPath) {
            newsItem.managedObjectContext?.performAndWait {
                cell.model = NewsFeedCellModel(withNewsFeedItem: newsItem)
            }
        }
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController?.sections?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultsController?.sections, sections.count > 0 {
            return sections[section].numberOfObjects
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let sections = fetchedResultsController?.sections, sections.count > 0 {
            return sections[section].name
        } else {
            return nil
        }
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return fetchedResultsController?.sectionIndexTitles
        
    }
    
    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return fetchedResultsController?.section(forSectionIndexTitle: title, at: index) ?? 0
    }
    
    // MARK: - NSFetchedResultsControllerDelegate
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert: tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete: tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default: break
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .fade)
        case .move:
            tableView.deleteRows(at: [indexPath!], with: .fade)
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        }
    }
    
    func controller(controller: NSFetchedResultsController<NSFetchRequestResult>, didChangeObject anObject: AnyObject, atIndexPath indexPath: IndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .fade)
        case .move:
            tableView.deleteRows(at: [indexPath!], with: .fade)
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
