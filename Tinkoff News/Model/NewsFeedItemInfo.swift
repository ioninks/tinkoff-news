//
//  NewsFeedItemInfo.swift
//  Tinkoff News
//
//  Created by Ионин Константин on 18/02/2018.
//  Copyright © 2018 Ионин Константин. All rights reserved.
//

import Foundation

struct NewsFeedItemInfo {
    let id: Int
    let text: String
    let publicationDate: Date
}
