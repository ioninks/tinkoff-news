//
//  NewsItemContentInfo.swift
//  Tinkoff News
//
//  Created by Ионин Константин on 18/02/2018.
//  Copyright © 2018 Ионин Константин. All rights reserved.
//

import Foundation

struct NewsItemContentInfo {
    
    let id: Int
    let text: String
}
