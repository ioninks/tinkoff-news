//
//  ModelExtensions.swift
//  Tinkoff News
//
//  Created by Ионин Константин on 18/02/2018.
//  Copyright © 2018 Ионин Константин. All rights reserved.
//

import Foundation
import CoreData

extension NewsFeedItem {
    
    class func addNewsFeedItem(
        withInfo newsFeedItemInfo: NewsFeedItemInfo,
        inContext context: NSManagedObjectContext) -> NewsFeedItem? {
        
        let request = NSFetchRequest<NewsFeedItem>(entityName: "NewsFeedItem")
        request.predicate = NSPredicate(format: "id = %@", NSNumber(value: newsFeedItemInfo.id))
        
        if let newsFeedItem = (try? context.fetch(request))?.first {
            return newsFeedItem
        } else if let newsFeedItem = NSEntityDescription.insertNewObject(forEntityName: "NewsFeedItem", into: context) as? NewsFeedItem {
            
            newsFeedItem.id = Int32(newsFeedItemInfo.id)
            newsFeedItem.text = newsFeedItemInfo.text
            newsFeedItem.publicationDate = newsFeedItemInfo.publicationDate
            return newsFeedItem
        }
        return nil
    }
}

extension NewsItemContent {
    
    class func addNewsItemContent(withInfo newsItemContentInfo: NewsItemContentInfo,
                                  inContext context: NSManagedObjectContext) -> NewsItemContent? {
        
        if let newsItemContent = fetch(withID: newsItemContentInfo.id, inContext: context) {
            return newsItemContent
        } else if let newsItemContent = NSEntityDescription.insertNewObject(forEntityName: "NewsItemContent", into: context) as? NewsItemContent {
            
            newsItemContent.id = Int32(newsItemContentInfo.id)
            newsItemContent.text = newsItemContentInfo.text
            return newsItemContent
        }
        return nil
    }
    
    class func fetch(withID id: Int, inContext context: NSManagedObjectContext) -> NewsItemContent? {
        
        let request = NSFetchRequest<NewsItemContent>(entityName: "NewsItemContent")
        request.predicate = NSPredicate(format: "id = %@", NSNumber(value: id))
        
        return (try? context.fetch(request))?.first
    }
}







