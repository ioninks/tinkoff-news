//
//  UIViewController+Extensions.swift
//  Tinkoff News
//
//  Created by Ионин Константин on 28/03/2018.
//  Copyright © 2018 Ионин Константин. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "ОК", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
}
